@isTest
public with sharing class NoteTrigger_Tester {

    private static String TEST_ACCOUNT_NAME = 'testerAcc1';

    @TestSetup
    static void makeData(){
        Account someAcc = new Account(Name=TEST_ACCOUNT_NAME);
        insert someAcc;
    }

    @isTest
    static void doesSyncCreatedNoteTranscribeNoteOnInsert()
    {
        Account someAcc = [SELECT Id FROM Account WHERE Name=:TEST_ACCOUNT_NAME];
        Note classicNote = new Note();
        classicNote.parentId = someAcc.Id;
        classicNote.Body = 'ClassicNoteBodyHere';
        classicNote.Title = 'ClassicNoteTitleHere';
        classicNote.isPrivate = false;

        test.startTest();
        insert classicNote;
        test.stopTest();

        // Check for new ContentNote
        ContentNote createdNote = [SELECT Id, Title, Content, SharingPrivacy FROM ContentNote WHERE Title=:classicNote.Title];
        System.assertEquals('N', createdNote.SharingPrivacy);

        // Check for new ContentDocumentLink
        List<ContentDocumentLink> createdDocLinks = [SELECT Id, ContentDocumentId, LinkedEntityId, LinkedEntity.Type
                                              FROM ContentDocumentLink
                                              WHERE ContentDocumentId=:createdNote.Id];
        for(ContentDocumentLink eachLink : createdDocLinks) {
            if(eachLink.LinkedEntity.Type == 'Account') {
                System.assertEquals(someAcc.Id, eachLink.LinkedEntityId);
            }
        }
        // Check for new Note Map
        Note_Map__c noteMap = [SELECT Classic_Note_Id__c, Enhanced_Note_Id__c FROM Note_Map__c LIMIT 1];
        System.assertEquals(classicNote.Id, noteMap.Classic_Note_Id__c);
        System.assertEquals(createdNote.Id, noteMap.Enhanced_Note_Id__c);
    }

    @isTest
    static void doesSyncDeletedNotePropogateDelete()
    {
        Account someAcc = [SELECT Id FROM Account WHERE Name=:TEST_ACCOUNT_NAME];
        // Insert 2 classic notes
        List<Note> classicNotes = new List<Note>();
        classicNotes.add(new Note(parentId=someAcc.Id, Body='Body1', Title='Title1', isPrivate=false));
        classicNotes.add(new Note(parentId=someAcc.Id, Body='Body2', Title='Title2', isPrivate=false));
        insert classicNotes;

        List<ContentNote> enhancedNotes = [SELECT Id, Title, Content, SharingPrivacy FROM ContentNote
                                          WHERE Title=:classicNotes[0].Title OR Title=:classicNotes[1].Title];
        List<ContentDocumentLink> createdDocLinks = [SELECT Id, ContentDocumentId, LinkedEntityId, LinkedEntity.Type
                                              FROM ContentDocumentLink
                                              WHERE ContentDocumentId=:enhancedNotes[0].Id OR ContentDocumentId=:enhancedNotes[1].Id];
        System.assertEquals(2, enhancedNotes.size());
        System.assertEquals(4, createdDocLinks.size()); // each ContentNote created link for User and Account
        

        test.startTest();
        delete classicNotes[0]; // delete 1 classic note
        test.stopTest();

        Id enhancedNoteOneId = enhancedNotes[0].Id;
        Id enhancedNoteTwoId = enhancedNotes[1].Id;

        // Check for presence of ONE ContentNote
        enhancedNotes = [SELECT Id, Title, Content, SharingPrivacy FROM ContentNote
                         WHERE Title=:classicNotes[0].Title OR Title=:classicNotes[1].Title];
        System.assertEquals(1, enhancedNotes.size());

        // Check for presnece of TWO ContentDocumentLinks
        createdDocLinks = [SELECT Id, ContentDocumentId, LinkedEntityId, LinkedEntity.Type
                                              FROM ContentDocumentLink
                                              WHERE ContentDocumentId=:enhancedNoteOneId OR ContentDocumentId=:enhancedNoteTwoId];
        System.assertEquals(2, createdDocLinks.size());
    }

    @isTest
    static void doesSyncUpdatedNotePropogate()
    {
        Account someAcc = [SELECT Id FROM Account WHERE Name=:TEST_ACCOUNT_NAME];
        // Insert 1 classic note, generates 1 enhanced note
        Note classicNote = new Note(parentId=someAcc.Id, Body='Body1', Title='Title1', isPrivate=false);
        insert classicNote;
        ContentNote enhancedNote = [SELECT Id, Title, Content, SharingPrivacy FROM ContentNote
                                          WHERE Title=:classicNote.Title];
        Id startEnhancedNoteId = enhancedNote.Id;
        String updatedTitle = 'UpdatedTitle1';

        Test.startTest();
        classicNote.Title = updatedTitle;
        update classicNote;
        Test.stopTest();

        // Check if related enhanced note was updated
        enhancedNote = [SELECT Id, Title, Content, SharingPrivacy FROM ContentNote
                                          WHERE Title=:updatedTitle];
        System.assertEquals(startEnhancedNoteId, enhancedNote.Id);
    }
}
