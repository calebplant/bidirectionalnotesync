public with sharing class ContentDocumentTrigger_Helper {
    
    public static void deleteRelatedNotes(List<ContentDocument> docs) {
        // Get Ids of deleted ContentNotes
        Set<Id> enhancedNotesIds = new Set<Id>();
        for(ContentDocument eachDoc : docs) {
            enhancedNotesIds.add(eachDoc.Id);
        }

        NoteConversionService.SyncDeletedContentNote(new List<Id>(enhancedNotesIds));
    }

    public static void updateRelatedNotes(List<ContentDocument> docs) {
        // Get updated ContentNotes
        Set<Id> enhancedNotesIds = new Set<Id>();
        for(ContentDocument eachDoc : docs) {
            enhancedNotesIds.add(eachDoc.Id);
        }
        List<ContentNote> updatedEnhancedNotes = [SELECT Id, Title, Content, SharingPrivacy
                                                FROM ContentNote WHERE Id IN :enhancedNotesIds];
                                                
        NoteConversionService.SyncUpdatedContentNote(updatedEnhancedNotes);
    }

    public static Boolean newDocumentIsContentNote(ContentDocument doc) {
        return doc.fileType == 'SNOTE';
    }
}
