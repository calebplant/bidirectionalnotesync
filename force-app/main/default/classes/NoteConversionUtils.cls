public with sharing class NoteConversionUtils {

    // Needed to convert Note body to ContentNote content
    // See: https://salesforce.stackexchange.com/questions/256335/note-cant-be-saved-error-on-contentnote-insert
    public static Blob sanitizeBodyForContentNote(String body) {
        if(String.isBlank(body)) {
            return Blob.valueOf('placeholder');
        }
        return Blob.valueOf(body.escapeXML().replace('\r\n', '<br>').replace('\r', '<br>').replace('\n', '<br>').replace('&apos;', '&#39;'));
    }

    public static String getValidTitle(String title) {
        return title != null && title.normalizeSpace().length() > 0 ? title : 'Untitled Note';
    }

    public static String sanitizeContentForNote(Blob content) {
        String result = content.toString().replace('<br>', '\\n'); // Add line breaks
        return content.toString().stripHtmlTags();
    }
}
