public with sharing class NoteTrigger_Helper {

    public static void convertToContentNote(List<Note> classicNotes) {
        NoteConversionService.SyncCreatedNote(classicNotes);
    }

    public static void deleteRelatedContentNotes(List<Note> classicNotes) {
        NoteConversionService.SyncDeletedNote(classicNotes);
    }

    public static void updateRelatedContentNote(List<Note> classicNotes) {
        NoteConversionService.SyncUpdatedNote(classicNotes);
    }
}
