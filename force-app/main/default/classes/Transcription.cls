public without sharing class Transcription {
    
    // Static variables to avoid recursion on trigger operation
    private static Boolean isTranscribing = false;

    
    public static boolean isTranscribing() {
        return isTranscribing;
    }

    public static void setTranscribing() {
        isTranscribing = true;
    }

    public static void setClosed() {
        isTranscribing = false;
    }
}
