@isTest
public with sharing class AttachmentTrigger_Test {

    private static String TEST_ACCOUNT_NAME_1 = 'testerAcc1';
    private static String TEST_ACCOUNT_NAME_2 = 'testerAcc2';

    @TestSetup
    static void makeData(){
        List<Account> accs = new List<Account>();
        accs.add(new Account(Name=TEST_ACCOUNT_NAME_1));
        accs.add(new Account(Name=TEST_ACCOUNT_NAME_2));
        insert accs;
    }
    
    @isTest
    static void doesCreateNewFilesPropogate()
    {
        Account acc1 = [SELECT Id FROM Account WHERE Name=:TEST_ACCOUNT_NAME_1];
        Account acc2 = [SELECT Id FROM Account WHERE Name=:TEST_ACCOUNT_NAME_2];
        
        List<Attachment> atts = new List<Attachment>();
        atts.add(new Attachment(Body=Blob.valueOf('body1'), Name='name1', parentId=acc1.Id));
        atts.add(new Attachment(Body=Blob.valueOf('body2'), Name='name2', parentId=acc2.Id));

        Test.startTest();
        insert atts;
        Test.stopTest();

        List<ContentVersion> actualCVs = [SELECT Id FROM ContentVersion];
        System.assertEquals(2, actualCVs.size());
    }
}
