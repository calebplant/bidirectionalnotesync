public with sharing class FileConversionService {
   
    public static void createNewFiles(List<Attachment> attachments)
    {
        System.debug('FileConversionService :: createNewFiles');

        // Insert new ContentNotes
        Map<Id, ContentVersion> newFileByAttachmentId = new Map<Id, ContentVersion>();
        for(Attachment eachAtt : attachments) {
            ContentVersion currentCV = new ContentVersion();

            currentCV.Title = eachAtt.Name;
            currentCV.PathOnClient = eachAtt.Name;
            currentCV.VersionData = eachAtt.Body;            
            newFileByAttachmentId.put(eachAtt.Id, currentCv);
        }
        upsert newFileByAttachmentId.values();

        // Link inserted ContentNotes to their corresponding parent
        Set<Id> addedCvIds = new Set<Id>();
        for(ContentVersion eachCV : newFileByAttachmentId.values()) {
            addedCvIds.add(eachCV.Id);
        }

        Map<Id, ContentVersion> newCvById = new Map<Id, ContentVersion>([SELECT ContentDocumentId FROM ContentVersion
                                                                        WHERE Id IN :addedCvIds]);
        
        Map<Id, Attachment> attachmentById = new Map<Id, Attachment>(attachments);
        List<ContentDocumentLink> docLinks = new List<ContentDocumentLink>();
        for(Id eachId : newFileByAttachmentId.keySet()) {

            ContentDocumentLink currentLink = new ContentDocumentLink();
            currentLink.ContentDocumentId = newCvById.get(newFileByAttachmentId.get(eachId).Id).ContentDocumentId;
            currentLink.LinkedEntityId = attachmentById.get(eachId).parentId;
            currentLink.ShareType = 'V';

            docLinks.add(currentLink);
        }
        upsert docLinks;
    }

    public static void createNewAttachments(List<ContentVersion> cvList, Map<Id, Id> linkedEntityIdByContentDocumentId)
    {
        System.debug('FileConversionService :: createNewAttachments');

        // Create new Attachments
        List<Attachment> attachmentsToInsert = new List<Attachment>();
        Map<String, Id> attachmentNameByParentId = new Map<String, Id>();
        for(ContentVersion eachCv : cvList) {
            Attachment newAttachment = new Attachment();
            newAttachment.Name = eachCv.PathOnClient;
            newAttachment.Body = eachCv.VersionData;
            newAttachment.parentId = linkedEntityIdByContentDocumentId.get(eachCv.ContentDocumentId);
            newAttachment.ContentType = eachCv.FileExtension;
            attachmentsToInsert.add(newAttachment);

            attachmentNameByParentId.put(newAttachment.Name, newAttachment.parentId);
        }

        // Ensure no duplicates
        List<Attachment> existingAtt = [SELECT Id, Name, ParentId
                                        FROM Attachment
                                        WHERE ParentId IN :attachmentNameByParentId.values()];
        System.debug('Existing Att: ' + existingAtt);
        Set<String> existingAttKeys = new Set<String>();
        for(Attachment eachAtt : existingAtt) {
            existingAttKeys.add('' + eachAtt.Name + eachAtt.ParentId);
        }
        System.debug('Insert before remove: ' + attachmentsToInsert);
        List<Attachment> filteredAttachmentsToInsert = new List<Attachment>();
        for(Attachment eachAtt : attachmentsToInsert) {
            if(!existingAttKeys.contains('' + eachAtt.Name + eachAtt.ParentId)) {
                filteredAttachmentsToInsert.add(eachAtt);
            }
        }
        upsert filteredAttachmentsToInsert;
    }
}
