public without sharing class NoteConversionService {

    /* Create ContentNotes corresponding to inserted Notes */
    public static void SyncCreatedNote(List<Note> classicNotes)
    {
        System.debug('NoteConversionService :: SyncCreatedNote');

        // Create ContentNotes
        Map<Id, ContentNote> enhancedNoteByClassicNoteId = new Map<Id, ContentNote>();
        for(Note eachClassicNote : classicNotes) {
            ContentNote enhancedNote = new ContentNote();
            enhancedNote.Title = eachClassicNote.Title;
            enhancedNote.Content = NoteConversionUtils.sanitizeBodyForContentNote(eachClassicNote.Body);
            enhancedNote.sharingPrivacy = eachClassicNote.isPrivate ? 'P' : 'N';

            enhancedNoteByClassicNoteId.put(eachClassicNote.Id, enhancedNote);
        }
        insert enhancedNoteByClassicNoteId.values();


        // Link new ContentNotes to appropriate parent
        List<ContentDocumentLink> docLinks = new List<ContentDocumentLink>();
        for(Note eachClassicNote : classicNotes) {
            ContentDocumentLink docLink = new ContentDocumentLink();
            docLink.LinkedEntityId = eachClassicNote.parentId;
            docLink.ContentDocumentId = enhancedNoteByClassicNoteId.get(eachClassicNote.Id).Id;
            docLink.shareType = 'V';
            docLinks.add(docLink);
        }
        insert docLinks;
 
        // Map enhanced note to classic note
        List<Note_Map__c> newMappings = new List<Note_Map__c>();
        for(Note eachClassicNote : classicNotes) {
            Note_Map__c newMap = new Note_Map__c();
            newMap.Classic_Note_Id__c = eachClassicNote.Id;
            newMap.Enhanced_Note_Id__c = enhancedNoteByClassicNoteId.get(eachClassicNote.Id).Id;
            newMappings.add(newMap);
        }
        System.debug('Inserting new Note Maps...');
        insert newMappings;
    }

    /* Create Notes corresponding to inserted ContentNotes */
    public static void SyncCreatedContentNote(List<ContentDocumentLink> docLinks, Map<Id, ContentNote> enhancedNotesById)
    {
        System.debug('NoteConversionService :: SyncCreatedContentNote');

        // Create new Notes
        Map<Id, Note> classicNotesByEnhancedNotesId = new Map<Id, Note>();
        for(ContentDocumentLink docLink : docLinks) {
            // Prevent creation of Note for User-related links
            Id entityId = docLink.LinkedEntityId;
            if('' + entityId.getSobjectType() != 'User') {
                Note classicNote = new Note();

                classicNote.parentId = docLink.LinkedEntityId;
                classicNote.Body = NoteConversionUtils.sanitizeContentForNote(enhancedNotesById.get(docLink.ContentDocumentId).Content);
                classicNote.Title = enhancedNotesById.get(docLink.ContentDocumentId).Title;
                classicNote.isPrivate = enhancedNotesById.get(docLink.ContentDocumentId).SharingPrivacy == 'P' ? true : false;
    
                classicNotesByEnhancedNotesId.put(docLink.ContentDocumentId, classicNote);
            }
        }
        System.debug('Inserting classic notes...');
        insert new List<Note>(classicNotesByEnhancedNotesId.values());

        // Insert map record connecting Note and ContentNote
        List<Note_Map__c> newMappings = new List<Note_Map__c>();
        for(Id eachEnhancedNoteId : classicNotesByEnhancedNotesId.keySet()) {
            Note_Map__c newMap = new Note_Map__c();
            newMap.Classic_Note_Id__c = classicNotesByEnhancedNotesId.get(eachEnhancedNoteId).Id;
            newMap.Enhanced_Note_Id__c = eachEnhancedNoteId;
            newMappings.add(newMap);
        }
        if(newMappings.size() > 0) {
            System.debug('Inserting new Note Maps...');
            insert newMappings;
        }
    }

    /* Delete ContentNotes corresponding to deleted Notes */
    public static void SyncDeletedNote(List<Note> deletedClassicNotes)
    {
        Set<Id> recordsToDelete = new Set<Id>();

        // Get ContentNote record Ids to delete
        Map<Id, Note> deletedClassicNotesMap = new Map<Id, Note>(deletedClassicNotes);
        List<Note_Map__c> classicNoteMap = [SELECT Id, Classic_Note_Id__c, Enhanced_Note_Id__c
                                             FROM Note_Map__c
                                             WHERE Classic_Note_Id__c IN :deletedClassicNotesMap.keySet()];
        for(Note_Map__c eachNoteMap : classicNoteMap) {
            recordsToDelete.add(eachNoteMap.Enhanced_Note_Id__c);
        }

        // Get related classic notes to delete (created by ContentNotes related to multiple records)
        List<Note_Map__c> noteMapsToDelete = [SELECT Id, Classic_Note_Id__c, Enhanced_Note_Id__c
                            FROM Note_Map__c
                            WHERE Classic_Note_Id__c IN :deletedClassicNotesMap.keySet()
                              OR Enhanced_Note_Id__c IN :recordsToDelete];
        for(Note_Map__c eachNoteMap : noteMapsToDelete) {
            // Only add classic note if it wasnt already deleted
            if(!deletedClassicNotesMap.containsKey(eachNoteMap.Classic_Note_Id__c)) { 
                recordsToDelete.add(eachNoteMap.Classic_Note_Id__c);
            }
        }
        // Delete ContentNotes, Notes, and Note Maps
        Database.delete(new List<Id>(recordsToDelete));
        delete noteMapsToDelete;
    }

    /* Delete Notes corresponding to deleted ContentNotes */
    public static void SyncDeletedContentNote(List<Id> deletedEnhancedNotesIds)
    {
        // Get note mappings for deleted notes
        List<Note_Map__c> noteMapsToDelete = [SELECT Id, Classic_Note_Id__c, Enhanced_Note_Id__c
                                             FROM Note_Map__c
                                             WHERE Enhanced_Note_Id__c IN :deletedEnhancedNotesIds];

        List<Id> classicNotesToDeleteIds = new List<Id>();
        for(Note_Map__c eachNoteMap : noteMapsToDelete) {
            classicNotesToDeleteIds.add(eachNoteMap.Classic_Note_Id__c);
        }

        // Delete ContentNotes and Note Maps
        Database.delete(classicNotesToDeleteIds);
        delete noteMapsToDelete;
    }

    /* Update ContentNotes corresponding to updated Notes */
    public static void SyncUpdatedNote(List<Note> updatedClassicNotes)
    {
        // Map classic notes by Id
        Map<Id, Note> classicNoteById = new Map<Id, Note>(updatedClassicNotes);
        // Get mapping between classic and enhanced notes
        List<Note_Map__c> noteMaps = [SELECT Id, Classic_Note_Id__c, Enhanced_Note_Id__c
                                FROM Note_Map__c
                                WHERE Classic_Note_Id__c IN :classicNoteById.keySet()];
        // Map enhanced notes by Id
        Set<Id> enhancedNoteIds = new Set<Id>();
        for(Note_Map__c eachMap : noteMaps) {
            enhancedNoteIds.add(eachMap.Enhanced_Note_Id__c);
        }
        Map<Id, ContentNote> enhancedNoteById = new Map<Id, ContentNote>([SELECT Title, Content, SharingPrivacy
                                                                          FROM ContentNote WHERE Id IN :enhancedNoteIds]);
        // Update each enhanced note
        List<ContentNote> enhancedNotesToUpdate = new List<ContentNote>();
        for(Note_Map__c eachMap : noteMaps) {
            Note classicNote = classicNoteById.get(eachMap.Classic_Note_Id__c);
            ContentNote enhancedNote = enhancedNoteById.get(eachMap.Enhanced_Note_Id__c);

            enhancedNote.Title = classicNote.Title;
            enhancedNote.Content = NoteConversionUtils.sanitizeBodyForContentNote(classicNote.Body);
            enhancedNote.sharingPrivacy = classicNote.isPrivate ? 'P' : 'N';
            enhancedNotesToUpdate.add(enhancedNote);
        }
        update enhancedNotesToUpdate;

        // Map classic note Id by enhanced note Id
        Map<Id, Id> classicIdByEnhancedId = new Map<Id, Id>();
        for(Note_Map__c eachMap : noteMaps) {
            classicIdByEnhancedId.put(eachMap.Enhanced_Note_Id__c, eachMap.Classic_Note_Id__c);
        }

        // Update related classic notes
        Set<Id> classicNotesToUpdateIds = new Set<Id>();
        List<Note_Map__c> relatedClassicNotesMap = [SELECT Id, Classic_Note_Id__c, Enhanced_Note_Id__c
                                                    FROM Note_Map__c
                                                    WHERE Enhanced_Note_Id__c IN :enhancedNoteById.keySet()
                                                    AND Classic_Note_Id__c NOT IN :classicNoteById.keySet()];

        Map<Id, Id> enhancedIdByClassicIdToUpdate = new Map<Id, Id>();
        for(Note_Map__c eachNoteMap : relatedClassicNotesMap) {
            classicNotesToUpdateIds.add(eachNoteMap.Classic_Note_Id__c);
            enhancedIdByClassicIdToUpdate.put(eachNoteMap.Classic_Note_Id__c, eachNoteMap.Enhanced_Note_Id__c);
        }
        List<Note> classicNotesToUpdate = [SELECT Id, Body, Title, IsPrivate
                                            FROM Note WHERE Id IN :classicNotesToUpdateIds];
        for(Note eachNote : classicNotesToUpdate) {
            Note classicNoteToCopy = classicNoteById.get(classicIdByEnhancedId.get(enhancedIdByClassicIdToUpdate.get(eachNote.Id)));
            
            eachNote.Body = classicNoteToCopy.Body;
            eachNote.Title = classicNoteToCopy.Title;
            eachNote.IsPrivate = classicNoteToCopy.IsPrivate;
        }
        update classicNotesToUpdate;
    }

    /* Update Notes corresponding to updated ContentNotes */
    public static void SyncUpdatedContentNote(List<ContentNote> updatedEnhancedNotes)
    {
        // Get mapping between enhanced and classic notes
        Map<Id, ContentNote> enhancedNoteById = new Map<Id, ContentNote>(updatedEnhancedNotes);
        List<Note_Map__c> noteMaps = [SELECT Id, Classic_Note_Id__c, Enhanced_Note_Id__c
                                FROM Note_Map__c
                                WHERE Enhanced_Note_Id__c IN :enhancedNoteById.keySet()];
        // Map classic notes by Id
        Set<Id> classicNoteIds = new Set<Id>();
        for(Note_Map__c eachMap : noteMaps) {
            classicNoteIds.add(eachMap.Classic_Note_Id__c);
        }
        Map<Id, Note> classicNoteById = new Map<Id, Note>([SELECT Title, Body, isPrivate
                                                            FROM Note WHERE Id IN :classicNoteIds]);
        // System.debug('Classic notes to update: ' + classicNoteById);
        
        // Update each classic note
        List<Note> classicNotesToUpdate = new List<Note>();
        for(Note_Map__c eachMap : noteMaps) {
            Note classicNote = classicNoteById.get(eachMap.Classic_Note_Id__c);
            ContentNote enhancedNote = enhancedNoteById.get(eachMap.Enhanced_Note_Id__c);

            // classicNote.Body = enhancedNote.Content.toString();
            classicNote.Body = NoteConversionUtils.sanitizeContentForNote(enhancedNote.Content);
            classicNote.Title = enhancedNote.Title;
            classicNote.isPrivate = enhancedNote.SharingPrivacy == 'P' ? true : false;
            classicNotesToUpdate.add(classicNote);
        }
        update classicNotesToUpdate;
    }
}