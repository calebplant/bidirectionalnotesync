public with sharing class ContentDocumentLinkTrigger_Helper {

    public static void convertContentNoteToNote(List<ContentDocumentLink> docLinks)
    {
        // Get associated ContentNotes
        Set<Id> enhancedNotesIds = new Set<Id>();
        for(ContentDocumentLink eachDoc : docLinks) {
            enhancedNotesIds.add(eachDoc.ContentDocumentId);
        }
        Map<Id, ContentNote> enhancedNotesById = new Map<Id, ContentNote>([SELECT Id, Content, Title, SharingPrivacy
                                                                           FROM ContentNote WHERE Id IN :enhancedNotesIds]);

        NoteConversionService.SyncCreatedContentNote(docLinks, enhancedNotesById);
    }

    public static void deleteRelatedNotes(List<ContentDocumentLink> docLinks) {
        // Get Ids of deleted ContentNotes
        Set<Id> enhancedNotesIds = new Set<Id>();
        for(ContentDocumentLink eachDoc : docLinks) {
            enhancedNotesIds.add(eachDoc.ContentDocumentId);
        }
 
        NoteConversionService.SyncDeletedContentNote(new List<Id>(enhancedNotesIds));
    }

    public static void convertFilestoAttachments(List<ContentDocumentLink> docLinks) {
        // Map LinkedEntity to ContentDocumentId (for use relating parentId later)
        Map<Id, Id> linkedEntityIdByContentDocumentId = new Map<Id, Id>();
        for(ContentDocumentLink eachDocLink : docLinks) {
            Id entityId = eachDocLink.LinkedEntityId;
            if('' + entityId.getSobjectType() != 'User') {
                linkedEntityIdByContentDocumentId.put(eachDocLink.ContentDocumentId, eachDocLink.LinkedEntityId);
            }
        }

        // Grab updated ContentVersions to fill in Attachment info
        List<ContentVersion> cvList = [SELECT Id, Title, PathOnClient, VersionData, ContentDocumentId, FileExtension
                                    FROM ContentVersion
                                    WHERE IsLatest=True AND ContentDocumentId IN :linkedEntityIdByContentDocumentId.keySet()];
        FileConversionService.createNewAttachments(cvList, linkedEntityIdByContentDocumentId);
    }

    public static Boolean newDocumentIsContentNote(ContentDocumentLink docLink) {
        Integer noteExists = [SELECT Id FROM ContentNote WHERE Id=:docLink.ContentDocumentId].size();
        return noteExists > 0;
    }
}
