@isTest
public with sharing class ContentDocumentTrigger_Test {

    
    private static String TEST_ACCOUNT_NAME_1 = 'testerAcc1';
    private static String TEST_ACCOUNT_NAME_2 = 'testerAcc2';

    @TestSetup
    static void makeData(){
        List<Account> accs = new List<Account>();
        accs.add(new Account(Name=TEST_ACCOUNT_NAME_1));
        accs.add(new Account(Name=TEST_ACCOUNT_NAME_2));
        insert accs;
    }
    
    @isTest
    static void doesSyncDeletedContentNotePropogateDelete()
    {
        Account someAcc = [SELECT Id FROM Account WHERE Name=:TEST_ACCOUNT_NAME_1];
        List<ContentNote> enhancedNotes = new List<ContentNote>();
        enhancedNotes.add(new ContentNote(Title='Title1', Content=Blob.valueOf('Content1')));
        enhancedNotes.add(new ContentNote(Title='Title2', Content=Blob.valueOf('Content2')));
        insert enhancedNotes;

        List<ContentDocumentLink> docLinks = new List<ContentDocumentLink>();
        docLinks.add(new ContentDocumentLink(LinkedEntityId=someAcc.Id, ShareType='V', ContentDocumentId=enhancedNotes[0].Id));
        docLinks.add(new ContentDocumentLink(LinkedEntityId=someAcc.Id, ShareType='V', ContentDocumentId=enhancedNotes[1].Id));
        insert docLinks;

        List<Note> classicNotes = [SELECT Id, Title, Body, ParentId FROM Note
                                  WHERE Title=:enhancedNotes[0].Title OR Title=:enhancedNotes[1].Title];
        System.assertEquals(2, classicNotes.size());

        System.debug('===========Start delete==============');

        Test.startTest();
        delete enhancedNotes[0]; //delete 1 ContentNote
        Test.stopTest();

        //Check for presence of ONE classic note
        classicNotes = [SELECT Id, Title, Body, ParentId FROM Note
                                  WHERE Title=:enhancedNotes[0].Title OR Title=:enhancedNotes[1].Title];
        System.assertEquals(1, classicNotes.size());
    }

    @isTest
    static void doesSyncUpdatedContentNotePropogate()
    {
        // Insert 1 enhanced note
        Account someAcc = [SELECT Id FROM Account WHERE Name=:TEST_ACCOUNT_NAME_1];
        ContentNote enhancedNote = new ContentNote(Title='Title1', Content=Blob.valueOf('Body1'));
        insert enhancedNote;
        ContentDocumentLink docLink = new ContentDocumentLink(LinkedEntityId=someAcc.Id, ContentDocumentId=enhancedNote.Id, ShareType='V');
        docLink.LinkedEntityId = someAcc.Id;
        docLink.shareType = 'V';
        insert docLink;
        // ensure classic note created
        Note classicNote = [SELECT Id, Title, Body, ParentId FROM Note WHERE Title=:enhancedNote.Title];
        Id classicNoteId = classicNote.Id;
        String updatedTitle = 'UpdatedTitle1';

        Test.startTest();
        System.debug('===========START UPDATE============');
        enhancedNote.Title = updatedTitle;
        update enhancedNote;
        Test.stopTest();
        
        classicNote = [SELECT Id, Title, Body, ParentId FROM Note WHERE Title=:updatedTitle];
        System.assertEquals(classicNoteId, classicNote.Id);
    }

    @isTest
    static void doesCreateNewAttachmentsPropogate()
    {
        // Insert enhanced note and relate it to two account records
        Account acc1 = [SELECT Id FROM Account WHERE Name=:TEST_ACCOUNT_NAME_1];
        ContentVersion newCv = new ContentVersion(Title='title1', PathOnClient='path1', VersionData=EncodingUtil.base64Decode('MyData'));
        ContentDocumentLink docLink = new ContentDocumentLink(LinkedEntityId=acc1.Id, ShareType='V');

        Test.startTest();
        insert newCv;
        Id newDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id=:newCv.Id].ContentDocumentId;
        docLink.ContentDocumentId = newDocId;
        insert docLink;
        Test.stopTest();

        List<Attachment> newAttachments = [SELECT Id, Name FROM Attachment];
        System.debug(newAttachments);
        System.assertEquals(1, newAttachments.size());
    }
}
