@isTest
public with sharing class ContentDocumentLinkTrigger_Test {

    private static String TEST_ACCOUNT_NAME_1 = 'testerAcc1';
    private static String TEST_ACCOUNT_NAME_2 = 'testerAcc2';

    @TestSetup
    static void makeData(){
        List<Account> accs = new List<Account>();
        accs.add(new Account(Name=TEST_ACCOUNT_NAME_1));
        accs.add(new Account(Name=TEST_ACCOUNT_NAME_2));
        insert accs;
    }

    @isTest
    static void doesSyncCreatedContentNoteTranscribeNoteOnInsertOneRelatedRecord()
    {
        Account someAcc = [SELECT Id FROM Account WHERE Name=:TEST_ACCOUNT_NAME_1];
        ContentNote enhancedNote = new ContentNote();
        enhancedNote.Title = 'EnhancedNoteTitleHere';
        enhancedNote.Content = Blob.valueOf('EnhancedNoteBodyHere');

        ContentDocumentLink docLink = new ContentDocumentLink();
        docLink.LinkedEntityId = someAcc.Id;
        docLink.shareType = 'V';

        Test.startTest();
        insert enhancedNote;
        docLink.ContentDocumentId = enhancedNote.Id;
        insert docLink;
        Test.stopTest();

        // Check for new classic note
        Note classicNote = [SELECT Id, Title, Body, ParentId FROM Note LIMIT 1];
        System.assertEquals(enhancedNote.Title, classicNote.Title);
        System.assertEquals('EnhancedNoteBodyHere', classicNote.Body);
        System.assertEquals(someAcc.Id, classicNote.parentId);

        // Check for new Note Map
        Note_Map__c noteMap = [SELECT Classic_Note_Id__c, Enhanced_Note_Id__c FROM Note_Map__c LIMIT 1];
        System.assertEquals(classicNote.Id, noteMap.Classic_Note_Id__c);
        System.assertEquals(enhancedNote.Id, noteMap.Enhanced_Note_Id__c);
    }

    @isTest
    static void doesSyncCreatedContentNoteTranscribeNoteOnInsertMultipleRelatedRecord()
    {
        // Insert enhanced note and relate it to two account records
        Account acc1 = [SELECT Id FROM Account WHERE Name=:TEST_ACCOUNT_NAME_1];
        Account acc2 = [SELECT Id FROM Account WHERE Name=:TEST_ACCOUNT_NAME_2];
        ContentNote enhancedNote = new ContentNote(Title='Title1',Content=Blob.valueOf('Body1'));

        List<ContentDocumentLink> docLinks = new List<ContentDocumentLink>();
        docLinks.add(new ContentDocumentLink(LinkedEntityId=acc1.Id, ShareType='V'));
        docLinks.add(new ContentDocumentLink(LinkedEntityId=acc2.Id, ShareType='V'));

        Test.startTest();
        insert enhancedNote;
        docLinks[0].ContentDocumentId = enhancedNote.Id;
        insert docLinks[0];
        docLinks[1].ContentDocumentId = enhancedNote.Id;
        insert docLinks[1];
        // insert docLinks;
        // ContentDocumentLink apparently handles one record at a time
        // See: https://salesforce.stackexchange.com/questions/228887/why-is-this-contentnote-trigger-receiving-1-record-at-a-time-even-though-multipl
        Test.stopTest();

        // Check for new classic notes
        // List<Note> classicNotes1 = [SELECT Id, Title, Body FROM Note WHERE ParentId=:acc1.Id];
        // List<Note> classicNotes2 = [SELECT Id, Title, Body FROM Note WHERE ParentId=:acc2.Id];
        // System.assertEquals(1, classicNotes1.size());
        // System.assertEquals(1, classicNotes2.size());
        List<Note> testNotes = [SELECT Id, title, body FROM Note];
        System.debug(testNotes);

        // Check for new Note Map
        List<Note_Map__c> noteMaps = [SELECT Classic_Note_Id__c, Enhanced_Note_Id__c FROM Note_Map__c];
        System.assertEquals(2, noteMaps.size());
    }
}