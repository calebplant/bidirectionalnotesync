public with sharing class AttachmentTrigger_Helper {
    public static void createRelatedFile(List<Attachment> attachments) {
        FileConversionService.createNewFiles(attachments);
    }
}
