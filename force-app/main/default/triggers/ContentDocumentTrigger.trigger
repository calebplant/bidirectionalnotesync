trigger ContentDocumentTrigger on ContentDocument (after delete, after update) {
    System.debug('START CDT... Is transcribing:' + Transcription.isTranscribing());

    if(Transcription.isTranscribing() == false) {
        Transcription.setTranscribing();

        if (Trigger.isAfter && Trigger.isDelete) {
            System.debug('IsAfterDelete');
            if(ContentDocumentTrigger_Helper.newDocumentIsContentNote(Trigger.Old[0])) {
                ContentDocumentTrigger_Helper.deleteRelatedNotes(Trigger.Old);
            }
        } else if (Trigger.IsAfter && Trigger.isUpdate) {
            System.debug('IsAfterUpdate');
            if(ContentDocumentTrigger_Helper.newDocumentIsContentNote(Trigger.New[0])) {
                ContentDocumentTrigger_Helper.updateRelatedNotes(Trigger.New);
            }
        }
        
        Transcription.setClosed();
    }
    System.debug('END CDT');
}