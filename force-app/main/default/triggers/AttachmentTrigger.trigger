trigger AttachmentTrigger on Attachment (after insert) {
    System.debug('START AT... Is transcribing:' + Transcription.isTranscribing());

    if(Transcription.isTranscribing() == false) {
        Transcription.setTranscribing();
        
        if(Trigger.isAfter && Trigger.IsInsert) {
            System.debug('IsAfterInsert');
            AttachmentTrigger_Helper.createRelatedFile(Trigger.New);
        }
        
        Transcription.setClosed();
    }
    System.debug('END AT');
}