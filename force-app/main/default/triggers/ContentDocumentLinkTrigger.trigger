trigger ContentDocumentLinkTrigger on ContentDocumentLink (after insert) {
    System.debug('START CDLT... Is transcribing:' + Transcription.isTranscribing());

    if(Transcription.isTranscribing() == false) {
        Transcription.setTranscribing();

        if(Trigger.isAfter && Trigger.isInsert) {
            System.debug('IsAfterInsert');
            if(ContentDocumentLinkTrigger_Helper.newDocumentIsContentNote(Trigger.New[0])) {
                System.debug('Is Note');
                ContentDocumentLinkTrigger_Helper.convertContentNoteToNote(Trigger.New);
            } 
            else {
                System.debug('Is Not Note');
                ContentDocumentLinkTrigger_Helper.convertFilestoAttachments(Trigger.New);
            }
        } 
        
        Transcription.setClosed();
    }

    System.debug('END CDLT');
}