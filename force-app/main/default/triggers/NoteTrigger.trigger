trigger NoteTrigger on Note (after insert, after delete, after update) {
    System.debug('NoteTrigger start. Is transcribing: ' + Transcription.isTranscribing());

    if(Transcription.isTranscribing() == false) {
        Transcription.setTranscribing();

        if(Trigger.isAfter && Trigger.isInsert) {
            NoteTrigger_Helper.convertToContentNote(Trigger.New);
        } else if(Trigger.isAfter && Trigger.isDelete) {
            NoteTrigger_Helper.deleteRelatedContentNotes(Trigger.Old);
        } else if(Trigger.isAfter && Trigger.isUpdate) {
            NoteTrigger_Helper.updateRelatedContentNote(Trigger.New);
        }
        
        Transcription.setClosed();
    }
    System.debug('NoteTrigger end');
}