After re-reading the task, it's possible I misunderstood part of the requirements. Currently, when a user **creates/deletes/edits** a Note/ContentNote, the changes propogate to a corresponding ContentNote/Note. However, uploading Attachments/Files currently propogate **creates** to a corresponding File/Attachment, but **not edits/deletes**. I did the Notes first, then reread the requirements and noticed it only mentioned handling creates. 

## Quick Links

### Triggers
* [AttachmentTrigger](force-app/main/default/triggers/AttachmentTrigger.trigger)
* [ContentDocumentLinkTrigger](force-app/main/default/triggers/ContentDocumentLinkTrigger.trigger)
* [ContentDocumentTrigger](force-app/main/default/triggers/ContentDocumentTrigger.trigger)
* [NoteTrigger](force-app/main/default/triggers/NoteTrigger.trigger)

### Trigger Helpers

* [AttachmentTrigger_Helper](force-app/main/default/classes/AttachmentTrigger_Helper.cls)
* [ContentDocumentLinkTrigger_Helper](force-app/main/default/classes/ContentDocumentLinkTrigger_Helper.cls)
* [ContentDocumentTrigger_Helper](force-app/main/default/classes/ContentDocumentTrigger_Helper.cls)
* [NoteTrigger_Helper](force-app/main/default/classes/NoteTrigger_Helper.cls)

### Services

* [FileConversionService](force-app/main/default/classes/FileConversionService.cls)
* [NoteConversionService](force-app/main/default/classes/NoteConversionService.cls)

### Tests

* [AttachmentTrigger_Test](force-app/main/default/classes/AttachmentTrigger_Test.cls)
* [ContentDocumentLinkTrigger_Test](force-app/main/default/classes/ContentDocumentLinkTrigger_Test.cls)
* [ContentDocumentTrigger_Test](force-app/main/default/classes/ContentDocumentTrigger_Test.cls)
* [NoteTrigger_Test](force-app/main/default/classes/NoteTrigger_Tester.cls)
